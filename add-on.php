<?php

	/*
		Vous pouvez récuperer l'UUID d'un joueur avec l'API Mojang
		https://api.mojang.com/users/profiles/minecraft/<username> (600 requetes/10 minutes)
	*/
	date_default_timezone_set('Europe/Paris');
	
	$uuid = "881f7305-85f6-4cb1-9323-a6441501af56"; //Chargable depuis une base de donnée (avec le pseudo,etc...)

	if (isset($_POST["skinRefresh"])) { //Si le joueur a appuyer sur le bouton "Rafraichir ?"
	    $lastRefreshDate = round(strtotime("17 November 2020 11:14") * 1000); //Date du dernier rafraichissement en millisecondes (Sauvegardable dans une base de donnée)
	    $currentDate = round(strtotime("now") * 1000); // Date actuel en millisecondes
		if (!isset($lastRefreshDate)) { refreshSkin($uuid); }
		if ($currentDate-$lastRefreshDate >= 86400000) { // Test si cela fais plus de 24h qu'il y a eu un refresh
            refreshSkin($uuid);
        } else { 
			$timeleft = 86400000-($currentDate-$lastRefreshDate);
			//Possibilitée d'afficher un message donnant le temps restant.
		}
	}

	function refreshSkin($uuid) {
		$url = 'https://mc-heads.net/head/'.$uuid.'/120'; // Utilisation du site mc-heads pour récuperer la tête du joueur.
		$path = $uuid.".png"; //Chemin menant à l'image téléchargée (https://ekalia.fr/assets/img/players/)
        if (file_exists($path)) { unlink($path); } //Suppression de l'ancienne image si cette dernière existe
		file_put_contents($path, file_get_contents($url)); //Création de la nouvelle image.
		// Mise à jour de la dernière date de refresh
	}
?>