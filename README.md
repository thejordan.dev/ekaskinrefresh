Hey, si tu lis ça c'est que tu es intéressé par mon idée d'ajout à la page de paramètres du site d'[Ekalia](https://ekalia.fr/).

Comme tu peux le voir ci-dessous mon idée est un bouton permettant de mettre à jour le skin d'un joueur si ce dernier viendrait à changer de skin

![alt Display screen](https://i.imgur.com/y94KdqP.png)

Je pense que c'est tout, est bien passé une bonne journée et merci de t'être intéressé à cette idée !!
